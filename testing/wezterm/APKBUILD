# Maintainer: Jakub Panek <me@panekj.dev>
pkgname=wezterm
pkgver=20220101.133340
_pkgver="20220101-133340-7edc5b5a"
pkgrel=0
pkgdesc="GPU-accelerated cross-platform terminal emulator and multiplexer written in Rust"
url="https://wezfurlong.org/wezterm/"
# s390x, riscv64 limited by rust
# ppc64le limited by ring https://github.com/briansmith/ring/issues/389
# armv7 armhf x86 limited by 32-bit incompatibility
arch="x86_64 aarch64"
license="MIT"
options="net"
depends="ncurses-terminfo-base"
makedepends="
	cargo
	fontconfig-dev
	libssh2-dev
	libx11-dev
	libxkbcommon-dev
	ncurses
	openssl-dev
	python3
	wayland-dev
	xcb-util-dev
	xcb-util-image-dev
	xcb-util-keysyms-dev
	xcb-util-wm-dev
	zlib-dev
	zstd-dev
	"
checkdepends="openssh"
source="
	https://github.com/wez/wezterm/releases/download/$_pkgver/wezterm-$_pkgver-src.tar.gz
	optimise.patch
	"
builddir="$srcdir/wezterm-$_pkgver"

prepare() {
	default_prepare
	cargo fetch --locked
}

build() {
	cargo build --release --frozen
	tic -x -o "$builddir"/terminfo "$builddir"/termwiz/data/wezterm.terminfo
}

check() {
	cargo test --frozen

	# required to unstuck CI
	killall -q sshd || true
}

package() {
	cargo install --frozen --offline --root="$pkgdir"/usr --path "$builddir"/wezterm
	cargo install --frozen --offline --root="$pkgdir"/usr --path "$builddir"/wezterm-gui
	cargo install --frozen --offline --root="$pkgdir"/usr --path "$builddir"/wezterm-mux-server

	install -Dm644 -t "$pkgdir"/usr/share/applications "$builddir"/assets/wezterm.desktop
	install -Dm644 -t "$pkgdir"/usr/share/metainfo "$builddir"/assets/wezterm.appdata.xml
	install -Dm644 "$builddir"/assets/icon/terminal.png "$pkgdir"/usr/share/pixmaps/wezterm.png
	install -Dm644 "$builddir"/assets/icon/wezterm-icon.svg "$pkgdir"/usr/share/pixmaps/wezterm.svg
	install -Dm644 "$builddir"/assets/icon/terminal.png "$pkgdir"/usr/share/icons/hicolor/128x128/apps/wezterm.png
	install -Dm644 "$builddir"/assets/icon/wezterm-icon.svg "$pkgdir"/usr/share/icons/hicolor/scalable/apps/wezterm.svg
	install -Dm644 "$builddir"/terminfo/w/wezterm "$pkgdir"/usr/share/terminfo/w/wezterm

	rm "$pkgdir"/usr/.crates*
}

sha512sums="
49bb6de1e199f8eb26a5f118020e3317245b174406fa8e254ae60dabec65f55182ee7c062aca2e670b22da6305e80c21b72d065b447f4d94044f38f71accb229  wezterm-20220101-133340-7edc5b5a-src.tar.gz
a5773816926eced20a228d28a5cf2f1609e91759c26e62a6a3af8e7daf28e7f116d7fc5fb4ea760277f7a7fc691e35bb72d3778e4ebf255afb1fb0088808e37f  optimise.patch
"
