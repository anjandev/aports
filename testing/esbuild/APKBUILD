# Contributor: Michał Polański <michal@polanski.me>
# Maintainer: Michał Polański <michal@polanski.me>
pkgname=esbuild
pkgver=0.14.18
pkgrel=0
pkgdesc="Extremely fast JavaScript bundler and minifier"
url="https://esbuild.github.io/"
license="MIT"
arch="all !riscv64" # blocked by nodejs
makedepends="go nodejs"
source="https://github.com/evanw/esbuild/archive/v$pkgver/esbuild-$pkgver.tar.gz"

export GOFLAGS="$GOFLAGS -trimpath -mod=readonly -modcacherw"
export GOPATH="$srcdir"

build() {
	go build \
		-ldflags="-X main.version=$pkgver" \
		-v ./cmd/esbuild

	node scripts/esbuild.js npm/esbuild/package.json --version
	node scripts/esbuild.js ./esbuild --neutral

	# binary path override
	sed -i '1s#^#var ESBUILD_BINARY_PATH = "/usr/bin/esbuild";\n#' \
		npm/esbuild/lib/main.js
}

check() {
	go test ./...
}

package() {
	install -Dm755 esbuild "$pkgdir"/usr/bin/esbuild

	local destdir=/usr/lib/node_modules/esbuild

	install -d \
		"$pkgdir"/$destdir/bin \
		"$pkgdir"/$destdir/lib

	install -Dm644 -t "$pkgdir"/$destdir npm/esbuild/package.json
	install -Dm644 -t "$pkgdir"/$destdir/lib npm/esbuild/lib/*
	ln -s /usr/bin/esbuild "$pkgdir"/$destdir/bin/esbuild
}

sha512sums="
aa6620bbe70ff72fd92cc8d05f2dd57fca725604355b65bdc1fdeb269e901f3b595b342fac76b5ebea5b5a2455c21a674e182a86de732cc13d7373b1423df7c0  esbuild-0.14.18.tar.gz
"
