# Contributor: Roberto Oliveira <robertoguimaraes8@gmail.com>
# Maintainer: Patrycja Rosa <alpine@ptrcnull.me>
pkgname=gradle
pkgver=7.3.3
pkgrel=0
pkgdesc="Build tool with a focus on build automation and support for multi-language development"
url="https://gradle.org/"
# mips64 and riscv64 blocked by openjdk8/java-jdk
arch="noarch !mips64 !riscv64"
options="!check"
license="Apache-2.0"
depends="java-jdk"
source="https://services.gradle.org/distributions/gradle-$pkgver-bin.zip"

# secfixes:
#   7.2-r0:
#     - CVE-2021-32751
#   6.8.3-r0:
#     - CVE-2020-11979

package() {
	local gradlehome="/usr/share/java/$pkgname"
	local destdir="$pkgdir/$gradlehome"

	install -dm755 "$destdir"/bin
	rm bin/*.bat
	install -m755 bin/* "$destdir"/bin

	install -dm755 "$pkgdir"/usr/bin
	ln -sf $gradlehome/bin/gradle "$pkgdir"/usr/bin/gradle

	install -dm755 "$destdir"/lib
	install -m644 lib/*.jar "$destdir"/lib

	install -dm755 "$destdir"/lib/plugins
	install -m644 lib/plugins/*.jar "$destdir"/lib/plugins

	# NOTICE file should be redistributed for derivative works
	local file; for file in LICENSE NOTICE; do
		install -m644 -D $file "$pkgdir"/usr/share/licenses/$pkgname/$file
	done
}

sha512sums="
1b383a99f02acd1ac7d442599e8f92967fb75318f016cfd796ef3b42a0b5d21bdf2b5803af1dd1614572a3a7575d2e5951007a20e55b01d42fa0c314bfd091f8  gradle-7.3.3-bin.zip
"
