# Contributor: Daniel Isaksen <d@duniel.no>
# Contributor: Victor Schroeder <me@vschroeder.net>
# Contributor: Marlus Saraiva <marlus.saraiva@gmail.com>
# Maintainer: Michal Jirků <box@wejn.org>
pkgname=elixir
pkgver=1.13.2
pkgrel=0
pkgdesc="Elixir is a dynamic, functional language designed for building scalable and maintainable applications"
url="https://elixir-lang.org/"
arch="noarch"
license="Apache-2.0"
depends="erlang-dev>=23.0 erlang-dialyzer"
subpackages="$pkgname-doc"
source="$pkgname-$pkgver.tar.gz::https://github.com/elixir-lang/elixir/archive/v$pkgver.tar.gz"

build() {
	LANG="en_US.UTF-8" make
}

check() {
	make test
	# test starts epmd, which then hangs presubmit pipeline.
	# so we kill it here.
	killall -q epmd
}

package() {
	make DESTDIR="$pkgdir" PREFIX=/usr install
}

sha512sums="
62fe42a9c3439d1fcba31ce4f9b0e489e6ad71417dc7de200b1651cd61a1fc5c433b1b9e0ecc293a5e5c279da47c909df45253193f6b4a8253043e07def05bc4  elixir-1.13.2.tar.gz
"
